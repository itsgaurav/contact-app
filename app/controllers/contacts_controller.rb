class ContactsController < ApplicationController
	before_action :authenticate_user!
	before_filter :load_contact, :only => [:show, :edit, :destroy, :update]
	
	def index
		@contacts = current_user.present? ? current_user.contacts : []
	end

	def show
	end

	def edit
	end

	def destroy
	end

	def create
		@contact = current_user.contacts.new(contact_params)
		if @contact.save
			redirect_to @contact, :notice => "Contact succeefully added"
		else
			render :new
		end
	end

	def update
		if @contact.update(contact_params)
      redirect_to @contact, notice: 'Contact was successfully updated.'
    else
      redirect_to edit_contact_path(@contact), notice: 'Could not update the Contact'
    end
	end

	def new
		@contact = current_user.contacts.new
	end

	private

	def load_contact
		@contact = Contact.find_by_id(params[:id]) or render_not_found
	end

	def contact_params
    params.require(:contact).permit(:name, :primary_number, :office_number, :city, :email, :address, :company, :dob, :user_id)
  end

end
