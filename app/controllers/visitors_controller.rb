class VisitorsController < ApplicationController

	def index
		redirect_to contacts_path if current_user.present?
	end
end
