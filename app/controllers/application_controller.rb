class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  protected

  def render_not_found
  	render :file => 'public/404.html', :status => :not_found, :layout => false if @contact.blank?
  end
end
