Contact App
================

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)


Ruby on Rails
-------------

This application requires:

- Ruby 2.3.0
- Rails 5.0.0.1

Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).

