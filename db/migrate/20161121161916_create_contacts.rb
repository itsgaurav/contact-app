class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
    	t.string :name
    	t.string :primary_number
    	t.string :office_number
    	t.string :city
    	t.string :email
    	t.string :address
    	t.string :company
    	t.date :dob
    	t.integer :user_id

    	t.timestamps null: false
    end

    add_index :contacts, [:user_id, :email], :unique => true
  end
end
